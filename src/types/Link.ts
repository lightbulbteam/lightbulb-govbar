interface Link {
    href: string;
    title: string;
    label: string;
    css_class: string;
}