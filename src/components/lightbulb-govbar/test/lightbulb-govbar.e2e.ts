import { newE2EPage } from '@stencil/core/testing';

describe('lightbulb-govbar', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<lightbulb-govbar></lightbulb-govbar>');

    const element = await page.find('lightbulb-govbar');
    expect(element).toHaveClass('hydrated');
  });
});
