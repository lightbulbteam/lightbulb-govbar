import { newSpecPage } from '@stencil/core/testing';
import { LightbulbGovbar } from '../lightbulb-govbar';

describe('lightbulb-govbar', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [LightbulbGovbar],
      html: `<lightbulb-govbar></lightbulb-govbar>`,
    });
    expect(page.root).toEqualHtml(`
      <lightbulb-govbar>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </lightbulb-govbar>
    `);
  });
});
