# lightbulb-govbar



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type     | Default   |
| ---------- | ---------- | ----------- | -------- | --------- |
| `language` | `language` |             | `string` | `'fr'`    |
| `theme`    | `theme`    |             | `string` | `'light'` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
