'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-5e0438e8.js');

/*
 Stencil Client Patch Browser v2.22.3 | MIT Licensed | https://stenciljs.com
 */
const patchBrowser = () => {
    const importMeta = (typeof document === 'undefined' ? new (require('u' + 'rl').URL)('file:' + __filename).href : (document.currentScript && document.currentScript.src || new URL('lightbulb-govbar.cjs.js', document.baseURI).href));
    const opts = {};
    if (importMeta !== '') {
        opts.resourcesUrl = new URL('.', importMeta).href;
    }
    return index.promiseResolve(opts);
};

patchBrowser().then(options => {
  return index.bootstrapLazy([["lightbulb-govbar.cjs",[[1,"lightbulb-govbar",{"language":[1],"theme":[1],"hostCssClasses":[32]}]]]], options);
});

exports.setNonce = index.setNonce;
