'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-5e0438e8.js');

/*
 Stencil Client Patch Esm v2.22.3 | MIT Licensed | https://stenciljs.com
 */
const patchEsm = () => {
    return index.promiseResolve();
};

const defineCustomElements = (win, options) => {
  if (typeof window === 'undefined') return Promise.resolve();
  return patchEsm().then(() => {
  return index.bootstrapLazy([["lightbulb-govbar.cjs",[[1,"lightbulb-govbar",{"language":[1],"theme":[1],"hostCssClasses":[32]}]]]], options);
  });
};

exports.setNonce = index.setNonce;
exports.defineCustomElements = defineCustomElements;
