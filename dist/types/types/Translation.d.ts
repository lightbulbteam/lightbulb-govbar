interface Translation {
  en: string;
  fr: string;
  de: string;
}
