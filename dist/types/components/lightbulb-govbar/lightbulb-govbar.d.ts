export declare class LightbulbGovbar {
  private allowedLanguages;
  language: string;
  validateLanguage(newValue: string): void;
  theme: string;
  private hostCssClasses;
  private translations;
  /**
   * Returns a translated string by id
   *
   * @param id
   */
  __(id: any): any;
  /**
   * Returns an array of links to be shown in the govbar
   *
   * @private
   */
  private getLinks;
  /**
   * Lifecycle Stencil Event. It is triggered when the component is loaded.
   */
  componentDidLoad(): void;
  /**
   * Adds specific classes to the provided classes array if the browser is Internet Explorer
   *
   * Function modified from this: http://stackoverflow.com/questions/19999388/check-if-user-is-using-ie-with-jquery
   *
   * @param classes: array
   */
  addInternetExplorerClasses(classes: any): any;
  /**
   * Returns the src (url) of the main government logo based on the current theme
   */
  getLogoSrc(): string;
  /**
   * Returns the srcset of the main government logo at different sizes based on the current theme
   */
  getLogoSrcSet(): string;
  /**
   * Return the title to be used for the logo anchor tag
   */
  getLogoTitle(): any;
  /**
   * Return the alt attribute to be used for the logo image
   */
  getLogoAlt(): string;
  /**
   * Return the url that the logo should point to
   */
  getLogoHref(): string;
  /**
   * Renders the component.
   */
  render(): any;
}
