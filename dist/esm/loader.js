import { p as promiseResolve, b as bootstrapLazy } from './index-dd496361.js';
export { s as setNonce } from './index-dd496361.js';

/*
 Stencil Client Patch Esm v2.22.3 | MIT Licensed | https://stenciljs.com
 */
const patchEsm = () => {
    return promiseResolve();
};

const defineCustomElements = (win, options) => {
  if (typeof window === 'undefined') return Promise.resolve();
  return patchEsm().then(() => {
  return bootstrapLazy([["lightbulb-govbar",[[1,"lightbulb-govbar",{"language":[1],"theme":[1],"hostCssClasses":[32]}]]]], options);
  });
};

export { defineCustomElements };
