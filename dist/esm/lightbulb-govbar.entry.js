import { r as registerInstance, h, H as Host } from './index-dd496361.js';

const lightbulbGovbarCss = ":host{display:block;border-bottom:1px solid #cccaca;font-family:Arial, sans-serif;font-size:12px;position:relative;z-index:100}:host:before,:host:after{content:\"\";display:table}:host:after{clear:both}:host .govbar-logo{border:0;display:block;padding:3px 8px;vertical-align:middle}:host .govbar-logo img{border:0;display:block;height:40px}:host .govbar-links{display:none;list-style:none;margin:0;padding:0}:host .govbar-link{border:0;border-top:1px solid #ddd;display:block;font-weight:bold;padding:8px;text-decoration:none}:host .govbar-more{font-weight:normal}@media only screen and (min-width: 45em){:host .govbar-logo{float:left}:host .govbar-links{display:block;float:right;line-height:30px}:host .govbar-links li{float:left;margin:0}:host .govbar-link{border-top:0;float:left;min-height:30px}}:host(.govbar--light){background:#fff}:host(.govbar--light) .govbar-link{color:#222 !important}:host(.govbar--light) .govbar-link:hover,:host(.govbar--light) .govbar-link:focus{background:#e6e6e6}:host(.govbar--dark){background:#333}:host(.govbar--dark) .govbar-link{color:#ddd !important}:host(.govbar--dark) .govbar-link:hover,:host(.govbar--dark) .govbar-link:focus{background:#666}:host(.ie7){display:inline-block}:host(.ie_lt9) .govbar-logo{float:left}:host(.ie_lt9) .govbar-links{display:block;float:right;line-height:30px}:host(.ie_lt9) .govbar-links li{float:left;margin:0}:host(.ie_lt9) .govbar-link{border-top:0;float:left;min-height:30px}";

const LightbulbGovbar = class {
  constructor(hostRef) {
    registerInstance(this, hostRef);
    this.allowedLanguages = [
      'en',
      'fr',
      'de'
    ];
    this.translations = new Map([
      [
        'government_of_the_grand_duchy_of_luxembourg',
        {
          'en': 'Government of the Grand Duchy of Luxembourg',
          'fr': 'Gouvernement du Grand-Duché de Luxembourg',
          'de': 'Regierung des Großherzogtums Luxemburg',
        }
      ],
      [
        'new_window',
        {
          'en': '(new window)',
          'fr': '(nouvelle fenêtre)',
          'de': '(neues Fenster)',
        }
      ],
      [
        'directory_of_public_luxembourg_website',
        {
          'en': 'Directory of public Luxembourg websites',
          'fr': 'L’annuaire des sites internets publics luxembourgeois',
          'de': 'Verzeichnis der staatlichen Internetseiten Luxemburg',
        }
      ],
      [
        'other_sites',
        {
          'en': 'Other sites',
          'fr': 'Autres sites',
          'de': 'Andere Seiten',
        }
      ],
    ]);
    this.language = 'fr';
    this.theme = 'light';
    this.hostCssClasses = '';
  }
  validateLanguage(newValue) {
    if (!this.allowedLanguages.includes(newValue)) {
      throw new Error('name: required');
    }
  }
  /**
   * Returns a translated string by id
   *
   * @param id
   */
  __(id) {
    return this.translations.get(id)[this.language];
  }
  /**
   * Returns an array of links to be shown in the govbar
   *
   * @private
   */
  getLinks() {
    return [
      {
        'href': 'http://luxembourg.lu/',
        'title': 'luxembourg.lu ' + this.__('new_window'),
        'label': 'luxembourg.lu',
        'css_class': 'govbar-link',
      },
      {
        'href': 'http://guichet.lu/',
        'title': 'guichet.lu ' + this.__('new_window'),
        'label': 'guichet.lu',
        'css_class': 'govbar-link',
      },
      {
        'href': 'http://gouvernement.lu/',
        'title': 'gouvernement.lu ' + this.__('new_window'),
        'label': 'gouvernement.lu',
        'css_class': 'govbar-link',
      },
      {
        'href': 'http://etat.lu/',
        'title': this.__('directory_of_public_luxembourg_website') + ' ' + this.__('new_window'),
        'label': this.__('other_sites'),
        'css_class': 'govbar-link govbar-more',
      },
    ];
  }
  ;
  /**
   * Lifecycle Stencil Event. It is triggered when the component is loaded.
   */
  componentDidLoad() {
    const cssClasses = [];
    cssClasses.push('govbar');
    cssClasses.push('govbar--' + this.theme);
    this.addInternetExplorerClasses(cssClasses);
    this.hostCssClasses = cssClasses.join(' ');
  }
  /**
   * Adds specific classes to the provided classes array if the browser is Internet Explorer
   *
   * Function modified from this: http://stackoverflow.com/questions/19999388/check-if-user-is-using-ie-with-jquery
   *
   * @param classes: array
   */
  addInternetExplorerClasses(classes) {
    const ua = window.navigator.userAgent;
    const msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      const version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      classes.push('msie');
      classes.push('ie' + version);
      if (version < 9) {
        classes.push('ie_lt9');
      }
    }
    const trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      const rv = ua.indexOf('rv:');
      const version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      classes.push('trident');
      classes.push('ie' + version);
    }
    const edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      const version = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
      classes.push('edge');
      classes.push('ie' + version);
    }
    return classes;
  }
  /**
   * Returns the src (url) of the main government logo based on the current theme
   */
  getLogoSrc() {
    return `//cdn.public.lu/pictures/logos/gov/gov-${this.theme}.png`;
  }
  /**
   * Returns the srcset of the main government logo at different sizes based on the current theme
   */
  getLogoSrcSet() {
    return `//cdn.public.lu/pictures/logos/gov/gov-${this.theme}-hdpi.png 1.5x,` +
      `//cdn.public.lu/pictures/logos/gov/gov-${this.theme}-xhdpi.png 2x,` +
      `//cdn.public.lu/pictures/logos/gov/gov-${this.theme}-xxhdpi.png 3x `;
  }
  /**
   * Return the title to be used for the logo anchor tag
   */
  getLogoTitle() {
    return this.__('government_of_the_grand_duchy_of_luxembourg');
  }
  /**
   * Return the alt attribute to be used for the logo image
   */
  getLogoAlt() {
    return this.__('government_of_the_grand_duchy_of_luxembourg') + ' ' + this.__('new_window');
  }
  /**
   * Return the url that the logo should point to
   */
  getLogoHref() {
    return 'http://gouvernement.lu';
  }
  /**
   * Renders the component.
   */
  render() {
    return (h(Host, { class: this.hostCssClasses }, h("div", null, h("a", { href: this.getLogoHref(), target: "_blank", class: "govbar-logo", title: this.getLogoTitle() }, h("img", { src: this.getLogoSrc(), srcset: this.getLogoSrcSet(), alt: this.getLogoAlt() })), h("ul", { class: "govbar-links" }, this.getLinks().map((link) => h("li", null, h("a", { class: link.css_class, href: link.href, title: link.title, target: "_blank" }, link.label)))))));
  }
  static get watchers() { return {
    "language": ["validateLanguage"]
  }; }
};
LightbulbGovbar.style = lightbulbGovbarCss;

export { LightbulbGovbar as lightbulb_govbar };
