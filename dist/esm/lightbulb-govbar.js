import { p as promiseResolve, b as bootstrapLazy } from './index-dd496361.js';
export { s as setNonce } from './index-dd496361.js';

/*
 Stencil Client Patch Browser v2.22.3 | MIT Licensed | https://stenciljs.com
 */
const patchBrowser = () => {
    const importMeta = import.meta.url;
    const opts = {};
    if (importMeta !== '') {
        opts.resourcesUrl = new URL('.', importMeta).href;
    }
    return promiseResolve(opts);
};

patchBrowser().then(options => {
  return bootstrapLazy([["lightbulb-govbar",[[1,"lightbulb-govbar",{"language":[1],"theme":[1],"hostCssClasses":[32]}]]]], options);
});
